#Peyton Oakes-Bryden

import car
myCar = car.Car("Z", 0, "Z")

newMake = input("Enter the make of your car: ")
newSpeed = int(input("Enter the speed of your car: "))
newYearModel = input("Enter the year of your car: ")
myCar.set_make(newMake)
myCar.set_speed(newSpeed)
myCar.set_year_model(newYearModel)
print("The make of your car is: ", myCar.get_make())
print("The speed of your car is: ", myCar.get_speed())
print("The year of your car is: ", myCar.get_year_model())

for x in range(0, 5):
  myCar.accelerate()
print("The new speed of your car is: ", myCar.get_speed())

for x in range(0, 5):
  myCar.brake()
print("The new speed of your car is: ", myCar.get_speed())
