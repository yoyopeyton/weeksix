__author__ = 'poakes2016'


import pet


myPet = pet.Pet("Fido","Dog",5)

print("The name of my pet is", myPet.get_name())
new_name = input("Enter a new name for your pet: ")
myPet.set_name(new_name)
print("The new name of my pet is", myPet.get_name())

print("The animal type of my pet is", myPet.get_animal_type())
new_animal_type = input("Enter a animal type for your pet: ")
myPet.set_animal_type(new_animal_type)
print("The new animal_Type of my pet is" , myPet.get_animal_type())


print("The age of my pet is", str(myPet.get_age()))
new_age = input("Enter a new age for your pet: ")
myPet.set_age(new_age)
print("The new age of my pet is" , myPet.get_age())
