#Peyton Oakes-Bryden
#Project 7
#8 October, 2015

import trivia

def qOne():
  print('Welcome to the Nasa trivia game! Get Going!')
  nC = 0
  questionOne = trivia.Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qOtQ = "How many bananas can a monkey eat in one hour?"
  qOp1 = "50"
  qOp2 = "100"
  qOp3 = "150"
  qOp4 = "0"
  qOnC = "B"

  questionOne.set_tQ(qOtQ)
  questionOne.set_p1(qOp1)
  questionOne.set_p2(qOp2)
  questionOne.set_p3(qOp3)
  questionOne.set_p4(qOp4)
  questionOne.set_nC(qOnC)

  print(questionOne.get_tQ())
  print("A. ", questionOne.get_p1())
  print("B. ", questionOne.get_p2())
  print("C. ", questionOne.get_p3())
  print("D. ", questionOne.get_p4())

  answer = input("What is the correct answer? ")
  if(answer == qOnC):
    nC += 1

  questionTwo = trivia.Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTtQ = "Which of these items can a dog eat?"
  qTp1 = "Avocados"
  qTp2 = "Raw Chicken"
  qTp3 = "Dark Chocolate"
  qTp4 = "Fermented Berries"
  qTnC = "D"

  questionTwo.set_tQ(qTtQ)
  questionTwo.set_p1(qTp1)
  questionTwo.set_p2(qTp2)
  questionTwo.set_p3(qTp3)
  questionTwo.set_p4(qTp4)
  questionTwo.set_nC(qTnC)

  print(questionTwo.get_tQ())
  print("A. ", questionTwo.get_p1())
  print("B. ", questionTwo.get_p2())
  print("C. ", questionTwo.get_p3())
  print("D. ", questionTwo.get_p4())

  answer = input("What is the correct answer? ")
  if(answer == qTnC):
    nC += 1

  questionThree = trivia.Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTHtQ = "How many eggs does a female walrus lay?"
  qTHp1 = "3"
  qTHp2 = "2"
  qTHp3 = "About 7"
  qTHp4 = "Silly Peyton, a walrus makes a web and spawns in some children with console commands"
  qTHnC = "D"

  questionThree.set_tQ(qTHtQ)
  questionThree.set_p1(qTHp1)
  questionThree.set_p2(qTHp2)
  questionThree.set_p3(qTHp3)
  questionThree.set_p4(qTHp4)
  questionThree.set_nC(qTHnC)

  print(questionThree.get_tQ())
  print("A. ", questionThree.get_p1())
  print("B. ", questionThree.get_p2())
  print("C. ", questionThree.get_p3())
  print("D. ", questionThree.get_p4())

  answer = input("What is the correct answer? ")
  if(answer == qTHnC):
    nC += 1

  questionFour = trivia.Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFtQ = "How many eyes does a duck have?"
  qFp1 = "10"
  qFp2 = "Enough"
  qFp3 = "The regular amount."
  qFp4 = "2"
  qFnC = "B"

  questionFour.set_tQ(qFtQ)
  questionFour.set_p1(qFp1)
  questionFour.set_p2(qFp2)
  questionFour.set_p3(qFp3)
  questionFour.set_p4(qTHp4)
  questionFour.set_nC(qFnC)

  print(questionFour.get_tQ())
  print("A. ", questionFour.get_p1())
  print("B. ", questionFour.get_p2())
  print("C. ", questionFour.get_p3())
  print("D. ", questionFour.get_p4())

  answer = input("What is the correct answer? ")
  if(answer == qFnC):
    nC += 1

  questionFive = trivia.Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFItQ = "Duck?"
  qFIp1 = "PRAISE BE!"
  qFIp2 = "Good..."
  qFIp3 = "Quack!"
  qFIp4 = "Goose!"
  qFInC = "A"

  questionFive.set_tQ(qFItQ)
  questionFive.set_p1(qFIp1)
  questionFive.set_p2(qFIp2)
  questionFive.set_p3(qFIp3)
  questionFive.set_p4(qFIp4)
  questionFive.set_nC(qFInC)

  print(questionFive.get_tQ())
  print("A. ", questionFive.get_p1())
  print("B. ", questionFive.get_p2())
  print("C. ", questionFive.get_p3())
  print("D. ", questionFive.get_p4())

  answer = input("What is the correct answer? ")
  if(answer == qFInC):
    nC += 1
  print("Congratulations! You got ", nC, " correct!")











qOne()
